from setuptools import setup

setup(
	name='feature_eng',
	author='Himanshu Swami',
	version='1.0.0',
	packages=['feature_eng'],
	package_dir={
		'feature_eng': './feature_eng'
	},
	install_requires=[],
	zip_safe=False
)